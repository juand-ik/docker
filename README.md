# Docker

Docker nos permite:

+ Desplegar / Escalar
+ Recrear / Destruir
+ Mantener la misma configuración

## Arquitectura

```

+--------------------------------------------------------+
|                                                        |
|                      Docker Host                       |
|                                                        |
|   +------------+ +----------+  +---------+ +-------+   |
|   | Contenedor | | Imágenes |  | Volumen | | Redes |   |
|   +----+-------+ +----+-----+  +----+----+ +---+---+   |
|        |              |             |          |       |
|        +--------------+----+--------+----------+       |
|                            |                           |
| +--------------------------+-------------------------+ |
| |                                                    | |
| |               Docker CLI - Cliente                 | |
| |                                                    | |
| +----------------------------------------------------+ |
| +----------------------------------------------------+ |
| |                      Rest API                      | |
| +----------------------------------------------------+ |
| +----------------------------------------------------+ |
| |                                                    | |
| |              Docker Daemon - Server                | |
| |                                                    | |
| +----------------------------------------------------+ |
+--------------------------------------------------------+

```

## Imagen

```

+--------------------------------------------------------+
|                                                        |
|                        Imagénes                        |
|                                                        |
|                                                        |
| +----------------------------------------------------+ |
| |                                                    | |
| |                       Capa 3 CMD                   | |
| |                                                    | |
| +----------------------------------------------------+ |
|                                                        |
| +----------------------------------------------------+ |        +-----------+
| |                                                    | |        |           |
| |                       Capa 2 RUN                   | +------->+ Read Only |
| |                                                    | |        |           |
| +----------------------------------------------------+ |        +-----------+
|                                                        |
| +----------------------------------------------------+ |
| |                                                    | |
| |                       Capa 1 FROM                  | |
| |                                                    | |
| +----------------------------------------------------+ |
|                                                        |
+--------------------------------------------------------+

```

## Contenedor

```
+--------------------------------------------------------+
|                                                        |
|                          Contenedor                    |
|                                                        |
|                                                        |
| +----------------------------------------------------+ |    +-------+
| |                                                    | |    |       |
| |      Ejecuión    Volumenes - Redes - Imagen        | +--->+ R & W |
| |                                                    | |    |       |
| +----------------------------------------------------+ |    +-------+
|                                                        |
| +----------------------------------------------------+ |
| |                                                    | |
| |                       Capa 3 CMD                   | |
| |                                                    | |
| +----------------------------------------------------+ |
|                                                        |
| +----------------------------------------------------+ |
| |                                                    | |
| |                       Capa . ...                   | |
| |                                                    | |
| +----------------------------------------------------+ |
|                                                        |
+--------------------------------------------------------+

```

## Notas

Construir imagenes, `docker-build` seguido del nombre de la imagen y un tag, que permite diferenciarla de otras similares(`tag`), por defecto estara *latest*
```
docker build --tag apache-centos .
docker build --tag apache-centos:base .
```

## Creando una imagen con CentOS y Apache

Agregar la primera capa al dockerfile, es aqui donde se indica cual sera la capa base(SO).
```
FROM centos
```
En la segunda capa se especifica que comandos se ejecutaran, `-y` contestara de forma afirmativa a los mensajes de la instalacion.
```
RUN yum install httpd -y
```
## Agredando CMD a la imagen
Si creamos un contenedor tal cual esta la imagen, este morira. Para que esta no termine, agregaremos apache a primer plano, con:
```
CMD apachectl -DFOREGROUND
```

\* Para revisar el historial de una imagen(lo que creo):
```
docker history -H nombredelaimagen
docker history -H apache-centos:base
```

## Creando un contendedor
Teniendo como base una imagen creada, ejecutamos:
```
docker run -d --name apache apache-centos
docker run -d --nombre-conte apache-centos:base  -> si tiene un tag
```

Especificando un puerto, el primero es el de nuestro equipo y el segundo es el del contenedor.
```
docker run -d --name apache -p 8080:80 apache-centos:apache-cmd
```

\* Finalizar un contenedor(forzar apagado)
```
docker kill container_id
docker kill 25ee6aa72abb
```

\* Remover un contenedor
```
docker rm -fv name
docker rm -fv apache
```

## Usando COPY/ADD
Copiando un directorio:
```
COPY CarpetaHTML /var/www/html
ADD CarpetaHTML /var/www/html
```

## Variables de Entorno
Crear variables, permite enviar contenido a las diferentes imagenes:
```
ENV variable contenido
RUN echo "$variable" > /var/www/html/prueba.html
```
## WorkDir
Especifica la ruta, donde estaremos ubicados:
```
WORKDIR /var/www
ENV variable contenido
RUN echo "$variable" > /html/prueba.html

```

## Expose
Perminte exponer cualquier puerto, esto puede requerir una configuracion extra.
```
EXPOSE 8080
```
## Label
Sirve para dar metadata a la imagen:
```
FROM centos

LABEL version=1.0
LABEL description="This is an apache image"
...
```
## User
Utiliza los diferentes usuario dentro del sistama:
```
FROM centos

RUN yum install httpd -y

ENV variable contenido
RUN echo "$variable" > /var/www/html/prueba.html

RUN echo "$(whoami)" > /var/www/html/user.html

RUN useradd juand
USER juand
RUN echo "$(whoami)" > /temp/user2.html

USER root
RUN cp /temp/user2.html /var/www/html/user2.html

CMD apachectl -DFOREGROUND
```

## Volumen
Permite tener la data de forma percistente en el contenedor, asi cuando el contenedor se elimine la data no lo haga tambien.
```
...

RUN echo "$(whoami)" > /temp/user2.html

VOLUMEN /var/www/html

USER root

...
```

## Buenas practicas

Al momento de crear imagenes, es bueno considerar lo siguiente:

+ Imagenes o servicios *Efimeros*(se puede destruir con gran facilidad).
+ *Un Servicio* por contendedor.
+ Build context -> .dockerignore(Es importante priorizar que archivos se van a agregar).
+ Pocas Capas(En medida de lo posible, hay que reducir el numero de capas que tiene la imagen).
+ Multilineas \ (Que pueda ser legible para otros usuarios).
+ Varios argumentos en una sola capa.
+ No instalar paquetes innecesarios.
+ Uso de Labels.



## Borrar Imágenes



Para borrar una imagen _*repositoryname:tag*_

`docker rmi anapsix/alpine-java:latest`



Eliminar varias imágenes:

`docker rmi anapsix/alpine-java:jdk8 nginx:1.13.10`



## Construir una imagen con un archivo

Docker tiene la posibilidad de crear imágenes desde otros archivos, por ejemplo:
`docker build -t tagtest -f my-dockerfile .`



## Danglings



Cuando una imagen, no tiene un nombre ni un tag `<none>` y esta se construye con el mismo nombre mas de una vez, crea un contendor al que se le denomina `dangling`.



Filtrando imágenes dangling:
`docker images -f dangling=true`



Eliminando una imagen dangling:

```
docker rmi tag
docker rmi 17d44fa7de97
```

Mostrar solo los id:

`docker images -f dangling=true -q`



Eliminar varias imágenes dangling(UNIX):

`docker images -f dangling=true -q | xargs docker rmi`



# Contenedores de Docker



## Mapeo de puertos

Docker permite especificar un puerto, a la hora de levantar una imagen. El primer puerto(`-p`)  corresponde al de nuestro equipo, el segundo al del contenedor.

`docker run -d -p 8080:8080 jenkins`



## Renombrar un contenedor

De un contenedor previamente constriudo tomamos el nombre y haciendo de `rename` remplazamos el nombre.

```
docker rename Actualname NewName
docker rename romantic_ptolemy jenkin
```



## Detener / Reiniciar / Eliminar

Para realizar alguna accion como detener, reiniciar, eliminar, etc.. solo anteponemos el prefijo, seguido del nombre o id del contenedor.

```
docker stop name_OR_id
docker stop jenkins

docker star name_OR_id
docker start jenkins

docker restar name_OR_id
docker restart jenkins
```



## Entrar al shell de una imagen

Si queremos acceder a la terminal de una imagen:

\* Terminal interactiva (-ti)

```
docker exec -ti image bash
docker exec -ti jenkins bash

docker exec -ti jenkins sh
```

Entrar como usuario _root_:

```
docker exec -u root -ti image bash

docker exec -u root -ti image sh
```


## Mostrar logs de una imagen

Es posible accedera los logs de una imagen con:

```
docker logs name_image/id

o un tail con:

docker logs name_image/id -f
docker logs carrierlegacy_mgo-carrier-legacy_1 -f     <--- Ejemplo
```


## Variable de entorno

Es posible crear variables de entorno desde la creacion del contenedor:

`docker run -dti -e "variable=123" --name environment env`



## Contenedor MYSQL

Creando un contendor partiendo de una imagen `mysql` [mysql-Image](https://hub.docker.com/_/mysql):

```
docker run -d -p 3333:3306 --name my-db -e "MYSQL_ROOT_PASSWORD=12345678" -e "MYSQL_DATABASE=docker-db" -e "MYSQL_USER=docker-user" -e "MYSQL_PASSWORD=87654321" mysql:5.7
```



\* Log de la imagen sql: `sudo docker logs -f my-db`



## Contenedor Mongo

Usando la una imagen oficial:

`docker run -d --name my-mongo -p 27017:27017 mongo`

\* Si deseamos saber cuantos recuersos esta utilizando una imagen: `docker stats imagen/id`



## Contenedor POSTGRES

Creando un contendor partiendo de una imagen `mysql` [postgres-Image](https://hub.docker.com/_/postgres):

```
docker run -d --name postgres -e "POSTGRES_PASSWORD=123456" -e "POSTGRES_USER=docker" -e "POSTGRES_DB=docker-db" -p 5432:5432 postgres
```



## Copiar contenido desde un contenedor

Es posible copiar el contenido que se encuentra dentro de un contenedor, utilizando `docker-cli:`

```
docker cp imagen:/ruta/del/archivo/archivo.txt /ruta/destino

docker cp apache:/var/log/dpkg.log . --> El punto es para usar el mismo directorio
```



## Contenedor temporal

Se puede especificar que un contenedor sera temporal, y que este se destruya despues de que termine su ejecución.

```
docker run --rm -ti --name centos centos bash
```



## Volumenes



Hacer uso de volumenes permite, que podamos persistir cualquier _data_ que se encuentre en un contenedor, ejemplo informacion de una *base de datos*.

\* `/var/lib/mysql` es la ruta donde la imagen de [mysql](https://hub.docker.com/_/mysql) almacena la información.

```
docker run -d --name db -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=12345678" -v opt/mysql:/var/lib/mysql mysql:5.7
```



## Listar y eliminar Volumenes

Igual que con las imágenes, es posible que tambien se creen volumenes _danglings_(sin nombre o referencia).

listar volumenes:
`docker volume ls`

Listar volumnes aplicando el filtro dangling:
`docker volume ls -f dangling=true`

Eliminar los volumenes(unix):

```
docker volume ls -f dangling=true -q | xargs docker volume rm
```





# Docker Compose



Esta herramienta nos permite crear aplicaciones _*multicontenedor*_. 

* Contenedores
* Imágenes
* Volúmenes
* Redes



### Ejemplo:

Crear una carpeta y dentro el archivo _docker-compose.yml_:

```
mkdir docker-compose
cd docker-compose
touch docker-compose.yml
```

El archivo _yml_ esta compuesto por cuatro partes importantes:

* Version( [info](https://docs.docker.com/compose/compose-file/compose-versioning/) )

* Services

* Volumes(opcional)

* Networks(opcional)

  

_docker-compose.yml file:_

```dockerfile
version: '3'
services:
	web:
		container_name: ngnix_1
		ports: "8080:80"
		image: nginx
```



## Construir el contenedor - YML

Ejecuta, esto al mismo nivel donde se encuentra el archivo _.yml_:

```dockerfile
docker-compose up -d
```

eliminar el contenedor:

```dockerfile
docker-compose down
```


## Errors

* Es posible que haya que ejecutar `sudo` antes de `docker`, para evitar esto, se puede agregar el usuario a la lista de permisos:

```
sudo usermod -aG docker username

sudo usermod -aG docker juand
```
